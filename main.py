# Written by Forrest Hooker, 02/25/2023

from machine import ADC, Pin
from time import sleep
import network, esp

#Init hw setup, disable esp's radios (Helps with false positives)
esp.osdebug(None)
wlan = network.WLAN(network.STA_IF)
wlan.active(False)

#ESP Pin Setup (Correlates to D2, D4, etc. pins)
ledPins = [2, 4, 5]
numLeds = len(ledPins)
#Set analog pin to D34
analogPin = 34

#Configure vars for pins
leds = [Pin(pin, Pin.OUT) for pin in ledPins]
#Init RF Detector IC Pin(s)
adc = ADC(Pin(analogPin))
#Set RF IC attenuation to 11DB (There is a reason this is done, I just forget why)
adc.atten(ADC.ATTN_11DB)

#Init LED range such that LED 1 is lowest, LED 5 is highest
def set_led_level(level):
    for i in range(numLeds):
        if i <= level:
            leds[i].on()
        else:
            leds[i].off()

#Terrible Startup sequence, feel free to delete :)
leds[0].on()
sleep(.1)
leds[0].off()
sleep(.05)
leds[1].on()
sleep(.1)
leds[1].off()
sleep(.05)
leds[2].on()
sleep(.1)
leds[2].off()
sleep(.05)
leds[0].on
leds[0].on()
leds[1].on()
leds[2].on()
sleep(.5)
leds[1].off()
leds[0].off()
leds[2].off()


while True:
    #Read sensor value, boost slightly (Adds some pseudo-sensitivity)
    sensor_value = (adc.read() + 10)
    #Set the initial LED "strength" (Or rather how many LEDs should be set)
    led_level = int((sensor_value / 4095) * numLeds)    
    #Actually enable/disable the LEDs
    set_led_level(led_level)
    #This *might* not be necessary, I forget why I did this
    sleep(0.1)
