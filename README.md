# Olm - ESP32 + AD8317 RF Detector

Pet project I created with the intention of "Discovering" RF-Capable devices. There are some flaws here I never got around to fixing; I unfortunately lost the detector during a move and got busy with a new job. 

**Please note, I have not worked on this project in ages and don't remember much.** This biggest thing I do remember is that it needed some filtering and didn't seem to pick up 2.4 Ghz that well. An LNA is probably not a terrible idea.

## Requirements

- Any MicroPython-flashed device; here I was using an ESP32, but you could certainly use a Pico if you removed the bits about disabling the radio(s).
- An AD8317. These are still available on Amazon for ~$16USD.
- A 9V Battery. There's probably a better power source, but this is what I had on hand.
- Some time and patience; I have long since forgotten what resistors, LEDs, etc. I used. I'll do my best to provide a pinout from the few photos I have.

**DO NOT PLUG THE BATTERY IN WHILE USB IS CONNECTED. PLEASE. I LEARNED THIS THE HARDWARE.**

## Pinout:

| ESP | Components |
|-----|------------|
| VIN | 9v         |
| 3v3 | AD8317        |
| GND | LEDS, AD8317 VCC GND + OUT, 9V  |
| D2  | LED 1      |
| D4  | LED 2      |
| D5  | LED 3      |
| ... | ...        |
| D34 | AD8317 OUT |

Some things to note; I could find very little documentation for which pin was which, and I remember even less, so some experimentation might be in order. This pinout is from memory and bad photographs :)


